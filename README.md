# Score_system
An API providing the means to create, read, update, delete scores for courses. 

### Team 4 members:
* _Iva Zhelyazkova_ [IvaZ](https://gitlab.com/IvaZ)
* _Andrey Marev_ [amarev](https://gitlab.com/IvaZ)

### Project details:
The API provides the means to create, read, update, delete scores for courses. 
* It also supports creation/deletion of users: trainers (admins or not) or trainees. 
* Users can register and login.
* Users can be granted Admin rights
* It supports retrieval/creation/deletion of courses
* It supports retrieval/setting/removing of scores

### Getting started
* install node & clone the project
* `cd score_system`
* `npm i`
* create a .env file at root level
* `npm run:migration generate -- -n="migaration"`
* install mysql workbench & create schema "scoresdb"
* `npm run:migration run`
* `npm run start`
* test the API with your own data or use the database file to insert rows into the tables
* Enjoy!

### Public, Private & Admin areas
Public : 
* POST -> /auth/register 
* POST -> /auth/login
* GET -> /courses

Private :
* GET -> /users
* POST -> /courses
* GET -> /admin
* POST -> /admin 
* GET -> /enrollments
* GET -> /enrollments/score
* POST -> /enrollments/score

Admin :
* DELETE -> /admin
* DELETE -> /courses

### API routes & methods (detailed)
1. POST -> /auth/register  saves a user in the DB. \
e.g. POST -> /auth/register with Body {
"email": "mayor.west@acad.com",
"password" : "P@ss12345",
"firstName" : "Mayor",
"lastName" : "West"
} => saved

2. POST -> /auth/login authenticates the user and returns a JWT token. \
e.g. POST -> /auth/login with Body {
"email": "mayor.west@acad.com",
"password" : "P@ss12345",
} => eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InBldGVyLmdyaWZmaW5AYWNhZC5jb20iLCJpc0FkbWluIjp0cnVlLCJpYXQiOjE1NDY3NzcxMjQsImV4cCI6MTU0NzM4MTkyNH0.VPp-vR_UmqyYGBxhNLMtQqpxfoP7iYR8e8347-aadhM

3.  GET -> /users invoked by an authenticated user returns a list of all registered users. \
e.g. GET -> /users with Bearer token : eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InBldGVyLmdyaWZmaW5AYWNhZC5jb20iLCJpc0FkbWluIjp0cnVlLCJpYXQiOjE1NDY3NzcxMjQsImV4cCI6MTU0NzM4MTkyNH0.VPp-vR_UmqyYGBxhNLMtQqpxfoP7iYR8e8347-aadhM
=> [{
        "user_id": 1,
        "firstName": "Mayor",
        "lastName": "West",
        "email": "mayor.west@acad.com",
        "password": "$2b$10$mrt3jVV0z5XP0Nc9VR9/pOlJMke5ZQ.TSkILgNoEFjB/yMyOZyBci",
        "avatarUrl": "public\\uploads\\default.png",
        "isAdmin": false,
        "role": null
    }
]

4. POST -> /courses invoked by an authenticated user with a trainer role saves a course in the DB. \
e.g. POST -> /courses with valid bearer token and Body
{"name": "js1","startDate":"08-01-2019","endDate": "15-01-2019"} => Course js1 created
 

5. GET -> /courses returns a list of courses the user is enrolled in. \
e.g. GET -> /courses with valid bearer token => [
    {
        "course_id": 1,
        "name": "js1",
        "startDate": "08-01-2019",
        "endDate": "15-01-2019",
        "description": null
    }
]

6. GET -> /admin invoked by user with admin roles returns a list of users that are not admins \
e.g. GET -> /admin with valid bearer token and admin role => [
    {
        "user_id": 3,
        "email": "mayor.west@acad.com",
        "isAdmin": false
    }
]

7. POST -> /admin invoked by user with admin role promotes an user to admin \
e.g. POST -> /admin with valid bearer token and Body {"user_id": 3} => User #3 promoted to Admin

8. DELETE -> /admin invoked by user with admin role deletes an user by id and all associated entries with the user from the enrollments table \
e.g. DELETE -> /admin with valid bearer token and Body {"user_id": 3} => User #3 has been deleted

9. GET -> /enrollments invoked by authenticated user with trainee role returns all scores for the user \
e.g. GET -> /enrollments => [
    {
        "firstName": "Peter",
        "lastName": "Griffin",
        "enrollments": [
            {
                "course": "js1",
                "score": 2
            }
        ]
    }
]

10. GET -> /enrollments/score invoked by authenticated user with trainer role returns list of enrollments with no score \
e.g. GET -> /enrollments/score => [
    {
        "enrollment_id": 2,
        "firstName": "Brian",
        "lastName": "Griffin",
        "course": "js1",
        "score": null
    }
] 

11. POST -> /enrollments/score invoked by authenticated user with trainer role sets a score \
e.g. POST -> /enrollments/score with Body {"enrollment_id": 2, "score_id": 3} => On #2 has been set scoreId #3

12. DELETE -> /courses invoked by an authenticated user with an admin role removes a course from the DB and all associated entries from the enrollments table \
e.g. DELETE -> /courses with Body {"course_id": 1} => Course removed

### Score_system - Link to our project repository in *[gitlab.com](https://gitlab.com/IvaZ/score_system.git)*

### Other remarks: 
* Project menagament via *[trello.com](https://trello.com/b/Ml0sjtZk/scoressystem)*
* Project reporters: @StevenTsvetkov, @RosenUrkov and @vesheff


