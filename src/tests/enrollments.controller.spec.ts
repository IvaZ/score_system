import { EnrollmentsController } from './../enrollments/enrollments.controller';
import { EnrollmentsService } from './../enrollments/enrollments.service';

import { SetScoreDTO } from '../models/score/score-set.dto';

jest.mock('../enrollments/enrollments.service');

describe('EnrollmentsController', () => {

    it('should call EnrollmentsService setScore method', async () => {
        // Arrange

        const MockRepository = jest.fn(() => ({
            createQueryBuilder: jest.fn(() => ({
            query: jest.fn().mockReturnThis(),
            })),
            }))();
        const enrollmentsService: EnrollmentsService = new EnrollmentsService(MockRepository);
        const ctrl = new EnrollmentsController(enrollmentsService);
        const score = new SetScoreDTO();

        jest.spyOn(enrollmentsService, 'setScore').mockImplementation(() => {
            return 'scores';
        });

        // Act
        await ctrl.setscore(score);

        // Assert
        expect(enrollmentsService.setScore).toHaveBeenCalledTimes(1);
    });

    it('should wait EnrollmentsService getAllScoresOfTrainee to return value', async () => {
        // Arrange

        const MockRepository = jest.fn(() => ({
            createQueryBuilder: jest.fn(() => ({
                leftJoinAndSelect: jest.fn().mockReturnThis(),
                innerJoinAndSelect: jest.fn().mockReturnThis(),
                getMany: jest.fn().mockReturnThis(),
            })),
            }))();
        const enrollmentsService: EnrollmentsService = new EnrollmentsService(MockRepository);
        const ctrl = new EnrollmentsController(enrollmentsService);

        jest.spyOn(enrollmentsService, 'getAllScoresOfTrainee').mockImplementation(() => {
            return {};
        });

        // Act
        await ctrl.all();

        // Assert
        expect(enrollmentsService.getAllScoresOfTrainee).toReturn();
    });
});