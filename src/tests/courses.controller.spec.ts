import { CourseCreateDTO } from './../models/course/course-create.dto';
import { CoursesService } from './../courses/courses.service';
import { CoursesController } from './../courses/courses.controller';
import { CourseToDeleteDTO } from '../models/course/course-to-delete.dto';

describe('CoursesController', () => {
    let coursesService: CoursesService;
    let ctrl: CoursesController;
    beforeAll(async () => {
        coursesService = new CoursesService(null);
        ctrl = new CoursesController(coursesService);
    });

    it('should call CoursesService getAll method', async () => {
        // Arrange
        jest.spyOn(coursesService, 'getAll').mockImplementation(() => []);

        // Act
        await ctrl.all();

        // Assert
        expect(coursesService.getAll).toHaveBeenCalledTimes(1);
    });

    it('should call CoursesService createCourse method', async () => {
        // Arrange
        const course = new CourseCreateDTO();
        jest.spyOn(coursesService, 'createCourse').mockImplementation(() => {
            return course;
        });

        // Act
        await ctrl.addCourse(course);

        // Assert
        expect(coursesService.createCourse).toHaveBeenCalledTimes(1);
    });

    it('should throw if CoursesService createCourse method is used with invalid Course format', async () => {
        const course: CourseCreateDTO = {name: 'x', startDate: '1', endDate: '2'};
        jest.spyOn(coursesService, 'createCourse').mockImplementation(() => {
            return null;
        });
        let result: string;
        try {
            await ctrl.addCourse(course);
        } catch (e) {
            result = e.message;
        }

        expect(result).toBe('Invalid course format');
    });

    it('should call CoursesService dropCourse method', async () => {
        // Arrange
        const course = new CourseToDeleteDTO();
        jest.spyOn(coursesService, 'dropCourse').mockImplementation(() => {
            return '';
        });

        // Act
        await ctrl.removeCourse(course);

        // Assert
        expect(coursesService.dropCourse).toHaveBeenCalledTimes(1);
    });
});
