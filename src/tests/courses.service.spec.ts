import { CourseToDeleteDTO } from './../models/course/course-to-delete.dto';
import { Repository } from 'typeorm';
import { CoursesService } from './../courses/courses.service';
import { Course } from '../data/entities/Course.entity';
describe('CoursesService', () => {
    it('constructor should init a new CourseService', () => {
        // Arrange
        const coursesService = new CoursesService(null);
        // Act & Assert
        expect(coursesService).toEqual({coursesRepository: null});
    });

    it('getAll method should return CourseCreateDTO', async () => {
        // Arrange
        const coursesRepository: Repository<Course> = new Repository();
        const coursesService: CoursesService = new CoursesService(coursesRepository);
        jest.spyOn(coursesRepository, 'find').mockImplementation(() => []);
        // Act
        const result = await coursesService.getAll();
        // Assert
        expect(result).toEqual([]);
    });

    it('dropCourse method should make a query', async () => {
        // Arrange
        const coursesRepository: Repository<Course> = new Repository();
        const coursesService: CoursesService = new CoursesService(coursesRepository);
        const course = {course_id: 1};
        jest.spyOn(coursesService, 'dropCourse').mockImplementation(() => 'done');
        // Act
        const result = await coursesService.dropCourse(course);
        // Assert
        expect(result).toEqual('done');
    });
});