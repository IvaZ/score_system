import { UsersController } from './../users/users.controller';
import { UsersService } from './../common/core/users.service';

jest.mock('../common/core/users.service');

describe('UsersController', () => {
    let usersService: UsersService;
    let ctrl: UsersController;
    beforeAll(async () => {
        usersService = new UsersService(null);
        ctrl = new UsersController(usersService);
    });

    it('should call UsersService getAll method', async () => {
        // Arrange
        jest.spyOn(usersService, 'getAll').mockImplementation(() => []);

        // Act
        await ctrl.all();

        // Assert
        expect(usersService.getAll).toHaveBeenCalledTimes(1);
    });
});