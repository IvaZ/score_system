import { UsersService } from '../common/core/users.service';
import { JwtService } from '@nestjs/jwt';
import { Injectable, NotFoundException } from '@nestjs/common';
import { JwtPayload } from './../interfaces/jwt-payload';
import { UserLoginDTO } from '../models/score/user-login.dto';
import { GetUserDTO } from '../models/score/get-user.dto';

@Injectable()
export class AuthService {
  constructor(
    private readonly usersService: UsersService,
    private readonly jwtService: JwtService,
  ) { }

  public async signIn(user: UserLoginDTO): Promise<string> {
    const userFound: any = await this.usersService.signIn(user);
    if (userFound) {

      return this.jwtService.sign({ email: userFound.email, isAdmin: userFound.isAdmin });
    } else {
      return null;
    }
  }

  async validateUser(payload: JwtPayload): Promise<GetUserDTO> {
    return await this.usersService.validateUser(payload);
  }
}
