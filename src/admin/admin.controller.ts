import { Controller, Get, Post, Body, Delete, UseGuards } from '@nestjs/common';
import { AdminService } from './admin.service';
import { AuthGuard } from '@nestjs/passport';
import { AdminGuard } from '../common';
import { UserDeleteDTO } from '../models/score/user-delete.dto';
import { UserNotAdminDTO } from '../models/score/user-not-admin.dto';

@Controller('admin')
@UseGuards(AuthGuard(), AdminGuard)
export class AdminController {
    constructor(
        private readonly adminService: AdminService,
    ) { }

    @Get()
    async allNotAdmins() {
        return this.adminService.getAllNotAdmins();
    }

    @Post()
    async setAsAdmin(@Body() user: UserNotAdminDTO): Promise<string> {
        await this.adminService.makeAdmin(user);
        return `User #${user.user_id} promoted to Admin`;
    }

    @Delete()
    async deleteUsers(@Body() user: UserDeleteDTO): Promise<string> {
        await this.adminService.userDelete(user);
        return `User #${user.user_id} has been deleted`;
    }

}
