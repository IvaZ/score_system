import { Module } from '@nestjs/common';
import { AdminService } from './admin.service';
import { AdminController } from './admin.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Course } from 'src/data/entities/Course.entity';
import { CoreModule } from 'src/common/core/core.module';
import { AuthModule } from 'src/auth/auth.module';

@Module({
  imports: [TypeOrmModule.forFeature([Course]),
                CoreModule,
                AuthModule,
            ],
  providers: [AdminService],
  controllers: [AdminController],
})
export class AdminModule {}
