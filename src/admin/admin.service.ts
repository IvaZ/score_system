import { Injectable } from '@nestjs/common';
import { User } from 'src/data/entities/User.entity';
import { Repository, getConnection } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { GetUserDTO } from '../models/score/get-user.dto';
import { UserNotAdminDTO } from '../models/score/user-not-admin.dto';
import { UserDeleteDTO } from '../models/score/user-delete.dto';

@Injectable()
export class AdminService {
    constructor(
        @InjectRepository(User)
        private readonly userRepository: Repository<User>,
    ) {}

    async getAllNotAdmins(): Promise<GetUserDTO[]> {
        const notAdmins = this.userRepository.find({
            select: ['user_id', 'email', 'isAdmin'],
            where: {
                isAdmin: false,
            },
            order: {
                user_id: 'ASC',
            },
        });
        return notAdmins;
    }

    async makeAdmin(user: UserNotAdminDTO): Promise<string> {
        await getConnection()
            .createQueryBuilder()
            .update(User)
            .set({isAdmin: true})
            .where('user_id = :id', { id: user.user_id })
            .execute();
        return `User #${user.user_id} promoted to Admin`;
    }

    async userDelete(user: UserDeleteDTO): Promise<void> {
        await getConnection()
        .createQueryBuilder()
        .delete()
        .from(User)
        .where('user_id = :id', {id: user.user_id})
        .execute();
    }

}
