import { IsEmail, IsBoolean, IsNumber } from 'class-validator';

export class UserNotAdminDTO {
    @IsNumber()
    user_id: number;
}