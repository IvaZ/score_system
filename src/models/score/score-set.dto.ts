import { IsEmail, IsBoolean, IsNumber } from 'class-validator';

export class SetScoreDTO {

    @IsNumber()
    enrollment_id: number;
    @IsNumber()
    score_id: number;
}