import { IsNumber } from 'class-validator';

export class UserDeleteDTO {

  @IsNumber()
  user_id: string;
}
