import { IsString } from 'class-validator';

export class CourseCreateDTO {
    @IsString()
    name: string;

    @IsString()
    startDate: string;

    @IsString()
    endDate: string;
}