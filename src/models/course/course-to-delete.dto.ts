import { IsNumber } from 'class-validator';

export class CourseToDeleteDTO {
    @IsNumber()
    course_id: number;
}