import { RolesGuard } from './../common/guards/roles/roles.guard';
import { Controller, Get, Post, Body, UseGuards, ValidationPipe } from '@nestjs/common';
import { EnrollmentsService } from './enrollments.service';
import { SetScoreDTO } from '../models/score/score-set.dto';
import { AuthGuard } from '@nestjs/passport';
import { Roles } from '../common';

@Controller('enrollments')

export class EnrollmentsController {

    constructor(
        private readonly enrollmentsService: EnrollmentsService,
     ) { }
     @Get()
     @Roles('trainer')
     @UseGuards(AuthGuard(), RolesGuard)
     all() {
         return this.enrollmentsService.getAllScoresOfTrainee();
     }
     @Get('score')
     @Roles('trainer')
     @UseGuards(AuthGuard(), RolesGuard)
     allEnrollments() {
         return this.enrollmentsService.getAllEnrollmentsWithoutScore();
     }
     @Post('score')
     @Roles('trainer')
     @UseGuards(AuthGuard(), RolesGuard)
     async setscore(@Body(new ValidationPipe({
        transform: true,
        whitelist: true,
      })) score: SetScoreDTO): Promise<string> {
         await this.enrollmentsService.setScore(score);
         return `On #${score.enrollment_id} has been set scoreId #${score.score_id}`;
     }

}
