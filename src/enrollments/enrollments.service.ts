import { Enrollment } from './../data/entities/Enrollment.entity';
import { User } from './../data/entities/User.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, getRepository, getConnection } from 'typeorm';
import { SetScoreDTO } from '../models/score/score-set.dto';

@Injectable()
export class EnrollmentsService {
    constructor(
        @InjectRepository(Enrollment)
        private readonly enrollmentRepository: Repository<Enrollment>,
    ) { }

    async getAllScoresOfTrainee() {

        const result = await getRepository(User)
            .createQueryBuilder('users')
            .leftJoinAndSelect('users.enrollments', 'enrollments')
            .innerJoinAndSelect('enrollments.course', 'course')
            .innerJoinAndSelect('enrollments.score', 'score')
            .getMany();

        return result.map(element => ({
            firstName: element.firstName,
            lastName: element.lastName,
            enrollments: element.enrollments
                .map(enrollment_el => ({
                    course: enrollment_el.course.name,
                    score: enrollment_el.score.score,
                })),
        }));
    }

    async getAllEnrollmentsWithoutScore() {
        const enrollments = await this.enrollmentRepository
            .createQueryBuilder('enrollments')
            .where('enrollments.score_id IS NULL')
            .innerJoinAndSelect('enrollments.user', 'user')
            .innerJoinAndSelect('enrollments.course', 'course')
            .getMany();

        return enrollments.map(element => ({
            enrollment_id: element.enrollment_id,
            firstName: element.user.firstName,
            lastName: element.user.lastName,
            course: element.course.name,
            score: null,
        }));
    }

    async setScore(score: SetScoreDTO): Promise<string> {
        await getConnection()
            .query('update scoresdb.enrollments set score_id = ? where enrollment_id = ?;', [score.score_id, score.enrollment_id]);

        return `On #${score.enrollment_id} has been set score_id #${score.score_id}`;
    }
}