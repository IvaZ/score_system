import { Enrollment } from './../data/entities/Enrollment.entity';
import { Module } from '@nestjs/common';
import { EnrollmentsController } from './enrollments.controller';
import { EnrollmentsService } from './enrollments.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CoreModule } from '../common/core/core.module';
import { AuthModule } from '../auth/auth.module';

@Module({
  imports: [TypeOrmModule.forFeature([Enrollment]), CoreModule, AuthModule],
  providers: [EnrollmentsService],
  exports: [],
  controllers: [EnrollmentsController],
})
export class EnrollmentsModule {}
