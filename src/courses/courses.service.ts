import { Enrollment } from './../data/entities/Enrollment.entity';
import { CourseToDeleteDTO } from './../models/course/course-to-delete.dto';
import { CourseCreateDTO } from 'src/models/course/course-create.dto';
import { Course } from './../data/entities/Course.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, getConnection } from 'typeorm';
import { validate } from 'class-validator';

@Injectable()
export class CoursesService{
    constructor(
        @InjectRepository(Course)
        private readonly coursesRepository: Repository<Course>,
    ) {}

    async getAll(): Promise<CourseCreateDTO[]>  {
        const courses = this.coursesRepository.find({});
        return courses;
    }

    async createCourse(course: CourseCreateDTO): Promise<CourseCreateDTO> {
        const errors = await validate(course);
        if (!errors.length) {
            await this.coursesRepository.save(course);
            return course;
        }
        return null;
    }

    async dropCourse(course: CourseToDeleteDTO): Promise<void> {

        await getConnection()
        .createQueryBuilder()
        .delete()
        .from(Course)
        .where('course_id = :id', {id: course.course_id})
        .execute();

    }
}