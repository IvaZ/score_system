import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CoreModule } from './../common/core/core.module';
import { AuthModule } from './../auth/auth.module';
import { Course } from './../data/entities/Course.entity';
import { CoursesController } from './courses.controller';
import { CoursesService } from './courses.service';
import { User } from 'src/data/entities/User.entity';

@Module({
    imports: [TypeOrmModule.forFeature([User, Course]),
                CoreModule,
                AuthModule,
            ],
    providers: [CoursesService],
    exports: [],
    controllers: [CoursesController],
})
export class CoursesModule {}