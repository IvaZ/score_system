import { AdminGuard } from './../common/guards/roles/admin.guard';
import { CourseToDeleteDTO } from './../models/course/course-to-delete.dto';
import { Controller, Get, Post, Body, Delete, UseGuards } from '@nestjs/common';
import { CoursesService } from './courses.service';
import { CourseCreateDTO } from './../models/course/course-create.dto';
import { Roles, RolesGuard } from '../common';
import { AuthGuard } from '@nestjs/passport';

@Controller('courses')
export class CoursesController {
    constructor(
        private readonly coursesService: CoursesService,
    ) { }

    @Get()
    async all() {
        return this.coursesService.getAll();
    }

    @Post()
    @Roles('trainer')
    @UseGuards(AuthGuard(), RolesGuard)
    async addCourse(@Body() course: CourseCreateDTO): Promise<string>{
        const newCourse = await this.coursesService.createCourse(course);
        if (!newCourse) {
            throw new Error('Invalid course format');
        } else {
            return `Course ${course.name} created`;
        }
    }

    @Delete()
    @UseGuards(AuthGuard(), AdminGuard)
    async removeCourse(@Body() course: CourseToDeleteDTO): Promise<string> {
        await this.coursesService.dropCourse(course);
        return `Course removed`;
    }
}
