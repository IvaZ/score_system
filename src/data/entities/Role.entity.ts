import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import { User } from './User.entity';

@Entity({
    name: 'roles',
})

export class Role {
    @PrimaryGeneratedColumn()
    role_id: number;

    @Column()
    name: string;

    @OneToMany(type => User, user => user.role)
    users: User[];
}