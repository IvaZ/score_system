import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';

import { Enrollment } from './Enrollment.entity';

@Entity({
    name: 'courses',
})

export class Course {
    @PrimaryGeneratedColumn()
    course_id: number;

    @Column()
    name: string;

    @Column()
    startDate: string;

    @Column()
    endDate: string;

    @Column({
        nullable: true,
    })
    description: string;

    @OneToMany(type => Enrollment, enrollment => enrollment.course)
    enrollments: Enrollment[];

}
