import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';

import { Enrollment } from './Enrollment.entity';

@Entity({
    name: 'scores',
})

export class Score {
    @PrimaryGeneratedColumn()
    score_id: number;

    @Column()
    score: number;

    @Column({
        nullable: true,
    })
    description: string;

    @OneToMany(type => Enrollment, enrollment => enrollment.score)
    enrollments: Enrollment[];
}
