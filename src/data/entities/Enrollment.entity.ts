
import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn } from 'typeorm';
import { User } from './User.entity';
import { Course } from './Course.entity';
import { Score } from './Score.entity';

@Entity({
    name: 'enrollments',
})

export class Enrollment {
    @PrimaryGeneratedColumn()
    enrollment_id: number;

    @Column({
        nullable: true,
    })
    description: string;
    @ManyToOne(type => User, user => user.enrollments, {onDelete: 'CASCADE'})
    @JoinColumn({ name: 'user_id' })
    user: User;

    @ManyToOne(type => Course, course => course.enrollments, {onDelete: 'CASCADE'})
    @JoinColumn({ name: 'course_id' })
    course: Course;

    @ManyToOne(type => Score, score => score.enrollments)
    @JoinColumn({ name: 'score_id' })
    score: Score;
}
