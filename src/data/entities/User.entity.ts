import { Entity, PrimaryGeneratedColumn, Column, OneToOne, JoinColumn, OneToMany, ManyToOne } from 'typeorm';
import { IsEmail } from 'class-validator';
import { Role } from './Role.entity';
import { Enrollment } from './Enrollment.entity';

@Entity({
    name: 'users',
})
export class User {

    @PrimaryGeneratedColumn()
    user_id: number;

    @Column()
    firstName: string;

    @Column()
    lastName: string;

    @Column()
    @IsEmail()
    email: string;

    @Column()
    password: string;

    @Column({
        nullable: true,
    })
    avatarUrl: string;

    @Column({ default: false })
    isAdmin: boolean;

    @ManyToOne(type => Role, role => role.users, {eager: true})
    role: Role;

    @OneToMany(type => Enrollment, enrollment => enrollment.user)
    enrollments: Enrollment[];
}
