import {MigrationInterface, QueryRunner} from "typeorm";

export class FinalMigration1546618176667 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("CREATE TABLE `roles` (`role_id` int NOT NULL AUTO_INCREMENT, `name` varchar(255) NOT NULL, PRIMARY KEY (`role_id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `users` (`user_id` int NOT NULL AUTO_INCREMENT, `firstName` varchar(255) NOT NULL, `lastName` varchar(255) NOT NULL, `email` varchar(255) NOT NULL, `password` varchar(255) NOT NULL, `avatarUrl` varchar(255) NULL, `isAdmin` tinyint NOT NULL DEFAULT 0, `roleRoleId` int NULL, PRIMARY KEY (`user_id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `scores` (`score_id` int NOT NULL AUTO_INCREMENT, `score` int NOT NULL, `description` varchar(255) NULL, PRIMARY KEY (`score_id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `enrollments` (`enrollment_id` int NOT NULL AUTO_INCREMENT, `description` varchar(255) NULL, `user_id` int NULL, `course_id` int NULL, `score_id` int NULL, PRIMARY KEY (`enrollment_id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `courses` (`course_id` int NOT NULL AUTO_INCREMENT, `name` varchar(255) NOT NULL, `startDate` varchar(255) NOT NULL, `endDate` varchar(255) NOT NULL, `description` varchar(255) NULL, PRIMARY KEY (`course_id`)) ENGINE=InnoDB");
        await queryRunner.query("ALTER TABLE `users` ADD CONSTRAINT `FK_f32dd66b36a5aa53fc615781bed` FOREIGN KEY (`roleRoleId`) REFERENCES `roles`(`role_id`)");
        await queryRunner.query("ALTER TABLE `enrollments` ADD CONSTRAINT `FK_ff997f5a39cd24a491b9aca45c9` FOREIGN KEY (`user_id`) REFERENCES `users`(`user_id`) ON DELETE CASCADE");
        await queryRunner.query("ALTER TABLE `enrollments` ADD CONSTRAINT `FK_b79d0bf01779fdf9cfb6b092af3` FOREIGN KEY (`course_id`) REFERENCES `courses`(`course_id`) ON DELETE CASCADE");
        await queryRunner.query("ALTER TABLE `enrollments` ADD CONSTRAINT `FK_3abdee1b252734772f18376b241` FOREIGN KEY (`score_id`) REFERENCES `scores`(`score_id`)");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `enrollments` DROP FOREIGN KEY `FK_3abdee1b252734772f18376b241`");
        await queryRunner.query("ALTER TABLE `enrollments` DROP FOREIGN KEY `FK_b79d0bf01779fdf9cfb6b092af3`");
        await queryRunner.query("ALTER TABLE `enrollments` DROP FOREIGN KEY `FK_ff997f5a39cd24a491b9aca45c9`");
        await queryRunner.query("ALTER TABLE `users` DROP FOREIGN KEY `FK_f32dd66b36a5aa53fc615781bed`");
        await queryRunner.query("DROP TABLE `courses`");
        await queryRunner.query("DROP TABLE `enrollments`");
        await queryRunner.query("DROP TABLE `scores`");
        await queryRunner.query("DROP TABLE `users`");
        await queryRunner.query("DROP TABLE `roles`");
    }

}
